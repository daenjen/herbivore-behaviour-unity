﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public Item item;
    public int pickupTimer = 3;

    //locals 
    [SerializeField]
    private float stateTimer = 0f;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                stateTimer += Time.deltaTime;

                //Debug.Log("Picking up" + item.name);
                Debug.Log(stateTimer);

                if (stateTimer >= pickupTimer)
                {
                    bool wasPickUp = Inventory.instance.Add(item);

                    if (wasPickUp)
                    {
                            Destroy(this.gameObject);
                    }
                }
            }
            else if (Input.GetKeyUp(KeyCode.E))
            {
                stateTimer = 0;
            }
        }
    }
}

