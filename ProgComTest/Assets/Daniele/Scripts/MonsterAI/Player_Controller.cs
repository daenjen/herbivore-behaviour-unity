﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    public int moveSpeed=10;
    public int speedRotation=50;

   
    void Start()
    {
        
    }
   
    void Update()
    {
        Move();
        Rotation();
    }

    public void Move()
    {
        float zMove= Input.GetAxis("Vertical");

        transform.Translate(transform.forward * zMove * moveSpeed * Time.deltaTime, Space.World);
    }

    public void Rotation()
    {
        float xMove = Input.GetAxis("Horizontal");

        transform.Rotate(Vector3.up, xMove * speedRotation * Time.deltaTime, Space.World);
    }
}
