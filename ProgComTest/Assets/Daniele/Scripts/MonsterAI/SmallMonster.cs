﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AiState
{
    waiting,
    walking,
    alerting,
    escaping,
    hiding,
    returning,
    stunned,
    dying
}

public class SmallMonster : Monster
{
    public AiState actualState = AiState.waiting;

    [Header("Timers")]
    public float alertingTime = 5f;
    public float waitingTime = 4f;
    public float beaconingTime = 20f;
    public float stunnedTime = 2f;
    public float hidingTime = 3f;

    [Header("Ranges")]
    public float distance;
    public float detectRange = 6f;
    public float sight = 6f;
    public int spawnArea = 10;

    [Header("Controls")]
    public bool canMove = true;
    public bool debugMode = true;
    public bool isDead = false;
    public bool isStunned = false;
    public bool checkMove = false;

    [Header("GameObjects")]
    public Transform nest_0;
    public Transform nest_1;
    public Transform spawn;
    public Transform muzzleSmallMonster;

    [Header("Components")]
    MeshRenderer meshRenderer;
    BoxCollider boxCollider;

    [Header("Vectors")]
    Vector3 nests;
    Vector3 checkpoint;
    Vector3 playerLastPos;
    Vector3 destination;
    RaycastHit hit;
    Ray ray;

    //locals
    [SerializeField]
    private int RandomNest;
    private float stateTimer = 0f;

    void Start()
    {
        MonsterStart();
        meshRenderer = GetComponent<MeshRenderer>();
        boxCollider = GetComponent<BoxCollider>();
    }

    void Update()
    {
        Timers();
        GetPath();
    }

    #region actions

    public void Escape()
    {
        monsterTarget = nest_0.transform.position;
    }

    public void CheckPlayer()
    {
        Vector3 targetDirection = new Vector3(player.transform.position.x, 0, player.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z);
        if (PlayerDetect())
        {
            Vector3 newDirection0 = Vector3.RotateTowards(transform.forward, targetDirection, rotationSpeed * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDirection0);
        }
        else
        {
            Vector3 newDirection1 = Vector3.RotateTowards(transform.forward, playerLastPos - transform.position, rotationSpeed * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDirection1);
        }
    }

    public void CheckPathDistance()
    {
        if (path == null || path.Count <= 0)
        {
            if (actualState == AiState.walking)
            {
                changeState(AiState.waiting);
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, path[pathCounter].worldPosition) < 0.3f)
            {
                if (path.Count <= 1)
                {
                    if (actualState == AiState.walking)
                    {
                        changeState(AiState.waiting);
                    }
                }
                else
                    pathCounter++;
            }
        }
    }

    public void GetNextCheckpoint(float ray)
    {
        for (int i = 0; i < 50; i++)
        {
            Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * ray;
            randomDirection += transform.position;
            randomDirection.y = 0;
            Node n = pf.grid.NodeFromPoint(randomDirection);

            if (n.walkable)
            {
                monsterTarget = randomDirection;
                break;
            }
        }
    }

    public void SearchCloseNest()
    {
        //Distance lenght from smallMonster to nest_0
        float nest_0_Distance = (new Vector3(nest_0.transform.position.x, 0, nest_0.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).magnitude;

        //Distance lenght from smallMonster to nest_1
        float nest_1_Distance = (new Vector3(nest_1.transform.position.x, 0, nest_1.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).magnitude;

        if (nest_0_Distance < nest_1_Distance)
        {
            //MoveToTarget(nest_0.transform.position);   
            monsterTarget = nest_0.transform.position;
        }
        else if (nest_1_Distance < nest_0_Distance)
        {
            //MoveToTarget(nest_1.transform.position);
            monsterTarget = nest_1.transform.position;
        }
    }

    public void Spawn()
    {
        meshRenderer.enabled = true;
        boxCollider.enabled = true;
        changeState(AiState.returning);
    }

    public void Death()
    {
        //death
    }

    #endregion
    #region decisions

    public bool PlayerInRange(float sight)
    {
        Ray ray = new Ray(muzzleSmallMonster.transform.position, transform.forward);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, sight))
        {
            if (debugMode)
            {
                Debug.Log(hit.transform.gameObject);
            }
            if (hit.transform.tag == "Player")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public bool PlayerDetect()
    {
        distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance <= detectRange)
        {
            playerLastPos = new Vector3(player.transform.position.x, 0, player.transform.position.z);
            return true;
        }
        else
            return false;
    }

    public bool CheckpointReachCheck()
    {
        if (Vector3.Distance(transform.position, checkpoint) < 1)
        {
            changeState(AiState.waiting);
            return true;
        }
        else
            return false;
    }

    public bool FatalDamage()
    {
        if (health <= 0)
        {
            changeState(AiState.dying);
            return true;
        }
        else
            return false;
    }

    #endregion
    #region utilities

    public void changeState(AiState newState)
    {
        stateTimer = 0;

        if (newState == AiState.walking)
        {
            maxSpeed = maxPatrolSpeed;
        }
        else if (newState == AiState.escaping)
        {
            maxSpeed = maxRunSpeed;
        }
        else if (newState == AiState.returning)
        {
            maxSpeed = maxPatrolSpeed;
            monsterTarget = spawn.transform.position;
            GetPath();
        }

        actualState = newState;
    }

    public void Timers()
    {
        if (actualState == AiState.waiting)
        {
            stateTimer += Time.deltaTime;
            if (stateTimer > waitingTime / 2 && (path == null || path.Count <= 1))
            {
                GetNextCheckpoint(checkpointsRadarRay);
                //smallMonster.MoveToTarget(smallMonster.path[0].worldPosition);
            }
            else if (stateTimer > waitingTime)
            {
                changeState(AiState.walking);
            }
        }

        else if (actualState == AiState.alerting)
        {
            stateTimer += Time.deltaTime;
            if (stateTimer > alertingTime)
            {
                changeState(AiState.walking);
            }
        }

        //if (meshRenderer.enabled && boxCollider.enabled)
        //{
        //    stateTimer = 0f;
        //}

        else if (actualState == AiState.hiding)
        {
            stateTimer += Time.deltaTime;

            if (stateTimer >= hidingTime)
            {
                Spawn();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Nest" && actualState == AiState.escaping)
        {
            meshRenderer.enabled = false;
            boxCollider.enabled = false;
            rb.velocity = new Vector3(0, 0, 0);
            movementSpeed = 0;
            changeState(AiState.hiding);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        bool check = true;
        if (collision.gameObject.tag == "Bullets" && check == true)
        {
            TakeDamage(health);
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator Stun()
    {
        changeState(AiState.stunned);
        yield return new WaitForSeconds(stunnedTime);
        changeState(AiState.escaping);
    }

    private void OnDrawGizmosSelected()
    {
        if (debugMode)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, detectRange);
            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(spawnSmallMonster.transform.position, spawnArea);
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(muzzleSmallMonster.transform.position, muzzleSmallMonster.transform.forward * sight);
        }
    }
}
#endregion


