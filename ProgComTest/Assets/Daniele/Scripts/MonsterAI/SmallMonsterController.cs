﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMonsterController : MonoBehaviour
{
    private SmallMonster smallMonster;

    void Start()
    {
        smallMonster = GetComponent<SmallMonster>();    
    }

    void Update()
    {
        switch (smallMonster.actualState)
        {
            case AiState.waiting:
                //ACTION
                smallMonster.Decelerate();

                //DECISION
                if (smallMonster.PlayerDetect())
                {
                    smallMonster.changeState(AiState.alerting);
                }
                else if (smallMonster.health == 0)
                {
                    smallMonster.changeState(AiState.dying);
                }
                break;

            case AiState.walking:
                //ACTION               
                smallMonster.CheckPathDistance();
              
                //DECISION 
                if (smallMonster.PlayerDetect())
                {
                    smallMonster.changeState(AiState.alerting);
                }
                else if (smallMonster.actualState == AiState.walking)
                {
                    if (smallMonster.path != null && smallMonster.path.Count  > smallMonster.pathCounter)
                    {
                        smallMonster.MoveToTarget(smallMonster.path[smallMonster.pathCounter].worldPosition);
                    }
                }
                else if (smallMonster.health == 0)
                {
                    smallMonster.changeState(AiState.dying);
                }
                break;        

            case AiState.alerting:
                //ACTION
                smallMonster.Decelerate();
                smallMonster.CheckPlayer();
                //DECISION
                if (smallMonster.actualState == AiState.alerting && smallMonster.PlayerInRange(smallMonster.sight))
                {
                    smallMonster.changeState(AiState.escaping);
                }
                else if (smallMonster.health == 0)
                {
                    smallMonster.changeState(AiState.dying);
                }
                break;


            case AiState.escaping:
                //ACTION
                smallMonster.SearchCloseNest();

                smallMonster.CheckPathDistance();
                smallMonster.MoveToTarget(smallMonster.path[smallMonster.pathCounter].worldPosition);

                smallMonster.checkMove = false;
                //DECISION        
                if (smallMonster.health == 0)
                {
                    smallMonster.changeState(AiState.dying);
                }
                break;

            case AiState.hiding:
                //ACTION
                //DECISION
                break;

            case AiState.returning:
                //ACTION  
                smallMonster.CheckPathDistance();
                smallMonster.MoveToTarget(smallMonster.path[smallMonster.pathCounter].worldPosition);

                //DECISION
                if (smallMonster.PlayerDetect())
                {
                    smallMonster.changeState(AiState.alerting);
                }
                else if (smallMonster.CheckpointReachCheck())
                {
                    smallMonster.changeState(AiState.waiting);
                }
                else if (smallMonster.health == 0)
                {
                    smallMonster.changeState(AiState.dying);
                }
                break;

            case AiState.stunned:
                //ACTION

                //DECISION
                break;

            case AiState.dying:
                //ACTION
                //death animation
                //Destroy(gameobject);
                //DECISION
                break;
            default:
                break;
        }
    }
}
