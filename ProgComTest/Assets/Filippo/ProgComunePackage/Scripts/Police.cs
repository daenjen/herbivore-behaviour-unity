﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Police : MonoBehaviour
{
   [HideInInspector] public NavMeshAgent agent;

    [Header("Police Stats")]
    public float maxHealth = 100;
    public float health = 100;
    public float maxRange = 15;
    public float minRange = 13;
    public float attackRange = 13;

    [Header("Gun Fire Stats")]
    public float gunFireDamage = 10;
    private float gunFireCDTimer = 0;
    public float gunFireMovementSpeed = 150;
    public float gunFireLifeTime = 0;
    public float gunFireRate = 0.75f;
    public GameObject escapeDestination;
    public GameObject gunFire;
    public GameObject fireMuzzle;

    void Start()
    {
        
    }


    void Update()
    {
        LookAtPlayer();

        CheckPlayerDistance();

        AttackRange();

        Timers();
    }

    public void LookAtPlayer()
    {
        Vector3 targetDir = -new Vector3(transform.position.x, 0, transform.position.z) + new Vector3(GameManager.instance.player.transform.position.x, 0, GameManager.instance.player.transform.position.z);

        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetDir, 1f, 0.0f));
    }

    public void CheckPlayerDistance()
    {
        if(Vector3.Distance(transform.position, GameManager.instance.player.transform.position) > maxRange)
        {
            agent.isStopped = false;
            agent.SetDestination(GameManager.instance.player.transform.position);
        }
        else if(Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < minRange)
        {
            agent.isStopped = false;
            agent.SetDestination(escapeDestination.transform.position);
        }
        else
        {
            StopNavMesh();
        }
    }

    public void StopNavMesh()
    {
        agent.isStopped = true;
    }

    public void AttackRange()
    {
        if(Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < attackRange)
        {
            Fire();
        }
    }

    public void Timers()
    {
        if(gunFireCDTimer > 0)
        {
            gunFireCDTimer -= Time.deltaTime;
        }
        else
        {
            gunFireCDTimer = 0;
        }
    }

    public void Fire()
    {
        if(gunFireCDTimer == 0)
        {
            GameObject g = Instantiate(gunFire, fireMuzzle.transform.position, Quaternion.Euler(0, transform.eulerAngles.y, 0));
            gunFireCDTimer = gunFireRate;
            g.GetComponent<GunProjectile>().lifeTime = gunFireLifeTime;
            g.GetComponent<GunProjectile>().damage = gunFireDamage;
            g.GetComponent<GunProjectile>().movementSpeed = gunFireMovementSpeed;
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
