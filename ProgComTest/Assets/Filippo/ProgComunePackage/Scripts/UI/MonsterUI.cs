﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterUI : MonoBehaviour
{
    public Image healthBar;
    public Image staminaBar;

    Monster monster;

    void Start()
    {
        monster = GetComponent<Monster>();
    }
    
    void Update()
    {
        healthBar.fillAmount = monster.health / monster.maxHealth;
        staminaBar.fillAmount = monster.stamina / monster.maxStamina;
    }
}
