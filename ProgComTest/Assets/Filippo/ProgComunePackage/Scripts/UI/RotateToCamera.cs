﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCamera : MonoBehaviour
{
    public Camera camera;

    private Monster monster;


    void Start()
    {
        monster = GetComponentInParent<Monster>();
    }


    private void FixedUpdate()
    {
        transform.localEulerAngles = new Vector3(60, -monster.gameObject.transform.localEulerAngles.y, 0);
    }
}
