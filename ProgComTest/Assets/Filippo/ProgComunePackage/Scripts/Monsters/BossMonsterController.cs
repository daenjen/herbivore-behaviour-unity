﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMonsterController : MonoBehaviour
{
    private BossMonster monster;

    private void Start()
    {
        monster = GetComponent<BossMonster>();
    }

    void Update()
    {
        switch (monster.actualState)
        {
            case BossState.Idle:

                monster.Decelerate();
                monster.FindPlayer();

                break;
            case BossState.Patrolling:

                monster.FindPlayer();

                if (monster.actualState == BossState.Patrolling)
                {
                    if (monster.path.Count > monster.pathCounter)
                        monster.MoveToTarget(monster.path[monster.pathCounter].worldPosition);
                    monster.CheckPathDistance();
                }

                break;
            case BossState.Chasing:

                monster.FindPlayer();

                if (monster.actualState == BossState.Chasing && monster.SingleRay())
                    monster.AttackRange();

                if (monster.actualState == BossState.Chasing)
                {
                    if (monster.DoubleRay())
                    {
                        monster.MoveToTarget(monster.player.transform.position);
                    }
                    else if (monster.path.Count > monster.pathCounter)
                    {
                        monster.MoveToTarget(monster.path[monster.pathCounter].worldPosition);
                    }
                }

                break;
            case BossState.Reconing:

                monster.FindPlayer();

                break;
            case BossState.Attacking:

                monster.Decelerate();

                break;
            case BossState.Stunned:
                break;
            case BossState.Exhausted:
                break;
            case BossState.Beaconed:
                break;
            case BossState.Dying:
                break;
            case BossState.Dead:
                break;
            default:
                break;
        }
    }
}
