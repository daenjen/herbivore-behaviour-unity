﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossState
{
    Idle,
    Patrolling,
    Chasing,
    Reconing,
    Attacking,
    Stunned,
    Exhausted,
    Beaconed,
    Dying,
    Dead
}

public class BossMonster : Monster
{
    [Header("Boss Path Finding Stats")]
    //Public
    public float idleTime;
    public float reconTime;
    //Private
    private float idleTimer = 0;
    private float reconTimer = 0;

    [Header("Claw Attack Stats")]
    public int clawAttackDamage = 20;
    public int clawAttackRange = 5;

    [Header("Puke Stats")]
    public int pukeAttackRange = 30;
    public float pukeDamage = 10;
    public float pukeFireRate = 0.2f;
    public float pukeLifeTime = 5;
    public float pukeMovementSpeed = 100;
    public float pukeCD = 10f;
    private float pukeTimer = 0;
    private List<float> pukeRotationIncrementList;
    public float pukeRotationIncrement = 10;
    public GameObject pukeShot;
    public GameObject fireMuzzle;
    [HideInInspector] public GameObject leftRC;
    [HideInInspector] public GameObject rightRC;

    [Header("Boss State")]
    public BossState actualState;

    void Start()
    {
        MonsterStart();
        pukeRotationIncrementList = new List<float>();
    }

    void Update()
    {
        Timers();
        GetPath();
    }

    public void ChangeState(BossState newState)
    {
        idleTimer = 0;
        reconTimer = 0;

        if (newState == BossState.Chasing)
            maxSpeed = maxRunSpeed;
        else if (newState == BossState.Patrolling)
            maxSpeed = maxPatrolSpeed;
        else if (newState == BossState.Stunned)
            movementSpeed = 0;

        actualState = newState;
    }

    #region Boss Pathfinding Methods



    #endregion

    #region Boss Checks Methods

    public void CheckPathDistance()
    {
        if (path == null || path.Count <= 0)
        {
            if (actualState == BossState.Chasing && !SingleRay())
                ChangeState(BossState.Reconing);
            else if (actualState == BossState.Patrolling)
                ChangeState(BossState.Idle);
        }
        else
        {
            if (Vector3.Distance(transform.position, path[pathCounter].worldPosition) < 0.1f)
            {
                if (path.Count <= 1)
                {
                    if (actualState == BossState.Chasing && !SingleRay())
                        ChangeState(BossState.Reconing);
                    else if (actualState == BossState.Patrolling)
                        ChangeState(BossState.Idle);
                }
                else
                    pathCounter++;
            }
        }
    }

    public bool SingleRay()
    {
        RaycastHit hitL;
        Ray rayL = new Ray(leftRC.transform.position, player.transform.position - leftRC.transform.position);
        RaycastHit hitR;
        Ray rayR = new Ray(rightRC.transform.position, player.transform.position - rightRC.transform.position);

        if (Physics.Raycast(rayL, out hitL, Vector3.Distance(leftRC.transform.position, player.transform.position)))
        {
            if (hitL.collider.gameObject.tag == "Player")
            {
                return true;
            }
            else
            {
                if (Physics.Raycast(rayR, out hitR, Vector3.Distance(rightRC.transform.position, player.transform.position)))
                {
                    if (hitR.collider.gameObject.tag == "Player")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }
        else
            return false;
    }

    public bool DoubleRay()
    {
        RaycastHit hitL;
        Ray rayL = new Ray(leftRC.transform.position, player.transform.position - leftRC.transform.position);
        RaycastHit hitR;
        Ray rayR = new Ray(rightRC.transform.position, player.transform.position - rightRC.transform.position);

        if (Physics.Raycast(rayL, out hitL, Vector3.Distance(leftRC.transform.position, player.transform.position) + 5) &&
            Physics.Raycast(rayR, out hitR, Vector3.Distance(rightRC.transform.position, player.transform.position) + 5))
        {
            if (hitL.collider.gameObject.tag == "Player" && hitR.collider.gameObject.tag == "Player")
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public void FindPlayer()
    {
        if (actualState != BossState.Chasing)
        {
            if (SingleRay() && PlayerInFront() && PlayerDistance() < viewRange)
            {
                ChangeState(BossState.Chasing);
                monsterTarget = player.transform.position;
            }
        }
        else
        {
            if (SingleRay() && PlayerDistance() < viewRange)
                monsterTarget = player.transform.position;
            else
                if (path == null || path.Count <= 0)
                ChangeState(BossState.Reconing);
        }
    }

    #endregion

    #region Attacks Methods

    public void AttackRange()
    {
        if (Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < clawAttackRange)
        {
            StartCoroutine("ClawAttack");
        }
        else if (Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < pukeAttackRange && pukeTimer <= 0)
        {
            StartCoroutine("PukeAttack");
        }
    }

    public IEnumerator ClawAttack()
    {
        ChangeState(BossState.Attacking);
        Vector3 targetDir = -new Vector3(transform.position.x, 0, transform.position.z) + new Vector3(GameManager.instance.player.transform.position.x, 0, GameManager.instance.player.transform.position.z);
        Debug.Log("Claw");
        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetDir, 1f, 0.0f));
        //attackCollider.enabled = true;
        yield return new WaitForSeconds(2);
        //attackCollider.enabled = false;
        ChangeState(BossState.Chasing);
    }

    public IEnumerator PukeAttack()
    {
        ChangeState(BossState.Attacking);
        Vector3 targetDir = -new Vector3(transform.position.x, 0, transform.position.z) + new Vector3(GameManager.instance.player.transform.position.x, 0, GameManager.instance.player.transform.position.z);

        Debug.Log("Puke");
        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetDir, 1f, 0.0f));

        pukeRotationIncrementList.Add(0f);
        pukeRotationIncrementList.Add(pukeRotationIncrement);
        pukeRotationIncrementList.Add(-pukeRotationIncrement);

        for (int i = 0; i < 3; i++)
        {
            Vector3 rot = transform.localEulerAngles;
            int rand = Random.Range(0, pukeRotationIncrementList.Count - 1);
            rot.y += pukeRotationIncrementList[rand];
            pukeRotationIncrementList.Remove(pukeRotationIncrementList[rand]);
            FirePukeShot(rot);
            yield return new WaitForSeconds(pukeFireRate);
        }
        pukeTimer = pukeCD;

        ChangeState(BossState.Chasing);
    }

    public void FirePukeShot(Vector3 rotation)
    {
        GameObject g = Instantiate(pukeShot, fireMuzzle.transform.position, Quaternion.Euler(rotation));
        PukeShot p = g.GetComponent<PukeShot>();
        p.lifeTime = pukeLifeTime;
        p.damage = pukeDamage;
        p.movememntSpeed = pukeMovementSpeed;

    }

    #endregion

    public void Timers()
    {
        if (actualState == BossState.Idle)
        {
            idleTimer += Time.deltaTime;
            if (idleTimer > idleTime / 2 && (path == null || path.Count <= 1))
                GetNextCheckpoint(checkpointsRadarRay);
            else if (idleTimer > idleTime)
                ChangeState(BossState.Patrolling);
        }
        else if (actualState == BossState.Reconing)
        {
            reconTimer += Time.deltaTime;
            if (reconTimer > reconTime / 2 && (path == null || path.Count <= 1))
            {
                GetNextCheckpoint(checkpointsRadarRay);
                RotateToTarget(transform.position - new Vector3(0,0,5));
            }
            else if (reconTimer >= reconTime)
                ChangeState(BossState.Patrolling);
        }

        //Cooldowns

        if (pukeTimer > 0)
            pukeTimer -= Time.deltaTime;
        else
            pukeTimer = 0;
    }

    public IEnumerator Stun(float duration)
    {
        ChangeState(BossState.Stunned);
        yield return new WaitForSeconds(duration);
        ChangeState(BossState.Chasing);
    }
}
