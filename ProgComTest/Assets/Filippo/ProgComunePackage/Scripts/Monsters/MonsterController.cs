﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour
{

    private BossMonster monster;

    private void Start()
    {
        monster = GetComponent<BossMonster>();
    }

    void Update()
    {
        switch (monster.actualState)
        {
            case BossState.Idle:
                break;
            case BossState.Patrolling:
                break;
            case BossState.Chasing:
                break;
            case BossState.Reconing:
                break;
            case BossState.Attacking:
                break;
            case BossState.Stunned:
                break;
            case BossState.Exhausted:
                break;
            case BossState.Beaconed:
                break;
            case BossState.Dying:
                break;
            case BossState.Dead:
                break;
            default:
                break;
        }
    }
}
