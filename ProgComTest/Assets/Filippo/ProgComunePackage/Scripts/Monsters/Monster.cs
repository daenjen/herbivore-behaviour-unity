﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetPosition
{
    Front,
    NextTo,
    Back
}

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PathFinding))]
public class Monster : MonoBehaviour
{
    public static Action OnDeathAction;
    [HideInInspector] public Player player;

    [Header("Monster General Stats")]
    public float maxHealth = 100;
    public float health = 100;
    [Space(5)]
    public float maxStamina = 100;
    public float stamina = 100;
    public float staminaPercentDebuff = 0;
    public float staminaRegen;
    [Space(5)]
    public float movementSpeed;
    public int accelerationPerSecond;
    [HideInInspector] public int maxSpeed;
    public int maxPatrolSpeed;
    public int maxRunSpeed;
    [Range(0f, 1f)]
    public float rotationSpeed = 0.1f;
    public float viewRange = 50;
    [Space(5)]
    [HideInInspector] public bool hooked;
    [HideInInspector] public List<Hook> hooksAttached;

    //Pathfinding Variables
    [Header("General Pathfinding Variables")]
    public float checkpointsRadarRay = 10f;
    [HideInInspector] public int pathCounter = 0;
    [HideInInspector] public Vector3 monsterTarget;
    [HideInInspector] public PathFinding pf;
    [HideInInspector] public List<Node> path { get; set; }
    private TargetPosition targetPositionType;

    //[Space(5)]
    //public float damage = 10;
    //public float stunDuration = 1;
    //[Space(5)]
    //public float maxSpeedChasing = 15;
    //public float maxSpeedPatrolling = 15;
    //public float actualSpeed = 10;
    //public float acceleration = 3f;
    //[Space(5)]

    //[Header("Puke Stats")]
    //public float pukeDamage = 10;
    //public float pukeFireRate = 0.2f;
    //public float pukeLifeTime = 5;
    //public float pukeMovementSpeed = 100;
    //public float pukeCD = 10f;
    //private float pukeTimer = 0;
    //private List<float> pukeRotationIncrementList;
    //public float pukeRotationIncrement = 10;

    //[Header("Monster States Variables")]
    //public MonsterState monsterState;
    //[Space(5)]

    //[Header("Public GameObjects")]
    //private NavMeshAgent agent;
    //public GameObject raycastMuzzle;
    //public GameObject[] checkpoints;
    //public BoxCollider attackCollider;
    //public GameObject pukeShot;
    //public GameObject fireMuzzle;

    //private int actualCheckpoint = 0;
    //private float canMoveTimer = 0;
    //public bool canMove = false;

    [HideInInspector] public Rigidbody rb;

    private void Awake()
    {
    }

    public void MonsterStart()
    {
        pf = GetComponent<PathFinding>();
        player = GameManager.instance.player;
        rb = GetComponent<Rigidbody>();
        path = new List<Node>();
    }

    #region Movements Methods

    public void MoveToTarget(Vector3 target)
    {
        CheckTargetPosition(target);
        CheckAccelerations();
        RotateToTarget(target);
        rb.velocity = transform.forward * movementSpeed;
    }

    public void RotateToTarget(Vector3 target)
    {
        Vector3 targetDir = new Vector3(target.x, 0, target.z) - new Vector3(transform.position.x, 0, transform.position.z);
        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetDir, rotationSpeed, 0.0f));
        Debug.Log("HAHAHAHAHAHA");
    }

    public void CheckAccelerations()
    {
        switch (targetPositionType)
        {
            case TargetPosition.Front:
                Accelerate();
                break;
            case TargetPosition.NextTo:
                if (movementSpeed < maxSpeed / 2f)
                    Accelerate();
                else
                    Decelerate();
                break;
            case TargetPosition.Back:
                if (movementSpeed < maxSpeed / 5f)
                    Accelerate();
                else
                    Decelerate();
                break;
            default:
                break;
        }
    }

    public void CheckTargetPosition(Vector3 target)
    {
        Vector3 toOther = target - transform.position;

        if (Vector3.Dot(transform.forward, toOther) > 0.4f)
            targetPositionType = TargetPosition.Front;
        else if (Vector3.Dot(transform.forward, toOther) < 0)
            targetPositionType = TargetPosition.Back;
        else
            targetPositionType = TargetPosition.NextTo;
    }

    public void Accelerate()
    {
        if (movementSpeed + accelerationPerSecond * Time.deltaTime > maxSpeed)
            movementSpeed = maxSpeed;
        else
            movementSpeed += accelerationPerSecond * Time.deltaTime;
    }

    public void Decelerate()
    {
        if (movementSpeed - accelerationPerSecond * Time.deltaTime * 5 < 0)
            movementSpeed = 0;
        else
            movementSpeed -= accelerationPerSecond * Time.deltaTime * 5;
    }

    #endregion

    #region Pathfinding Methods

    public void GetPath()
    {
        if (monsterTarget != null && path != null)
        {
            pf.target = monsterTarget;
            path = pf.path;
        }
    }

    public void GetNextCheckpoint(float ray)
    {
        for (int i = 0; i < 50; i++)
        {
            Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * ray;
            randomDirection += transform.position;
            randomDirection.y = 0;
            Node n = pf.grid.NodeFromPoint(randomDirection);

            if (n.walkable)
            {
                monsterTarget = randomDirection;
                break;
            }
        }
    }

    #endregion

    #region Monster Checks

    public bool RaycastToPlayer()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, player.transform.position - transform.position);

        if (Physics.Raycast(ray, out hit, Vector3.Distance(transform.position, player.transform.position)))
        {
            if (hit.transform.gameObject == player.gameObject)
                return true;
            else
                return false;
        }
        else
            return true;

    }

    public bool PlayerInFront()
    {
        Vector3 toOther = player.transform.position - transform.position;

        if (Vector3.Dot(transform.forward, toOther) > 0.0f)
            return true;
        else
            return false;
    }

    public float PlayerDistance()
    {
        return Vector3.Distance(transform.position, player.transform.position);
    }

    #endregion

    #region Stats Management

    public void TakeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
            if (health <= 0)
            {
                if (OnDeathAction != null)
                    OnDeathAction();
            }
        }
    }

    public void LoseStamina(float staminaLost)
    {
        stamina -= staminaLost + (staminaPercentDebuff / 100);
        if (stamina <= 0)
        {
            stamina = 0;
        }
    }

    public void SetStaminaDebuff()
    {
        if (stamina < maxStamina / 2)
        {
            staminaPercentDebuff = maxStamina / 2 - (maxStamina) / 100 * stamina;
        }
        else
        {
            staminaPercentDebuff = 0;
        }
    }

    public void StaminaRegen()
    {
        if (stamina < maxStamina)
        {
            stamina += (staminaRegen - (staminaPercentDebuff / 100)) * Time.deltaTime;
        }
        else
        {
            stamina = maxStamina;
        }
    }

    #endregion
}
