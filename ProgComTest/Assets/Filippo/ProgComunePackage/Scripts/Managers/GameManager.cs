﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public Player player;

    [Header("Lists of hook's collisions")]

    public List<string> interactableSolid = new List<string>();
    public List<string> interactableDynamic = new List<string>();
    public List<string> instantDynamic = new List<string>();

    void Awake()
    {
        instance = this;
        
    }


    void Update()
    {
        
    }
}
