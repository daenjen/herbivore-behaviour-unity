﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerStates
{
    Idle,
    Stunned,
    HookInteracting,
    Dying,
    Died
}

public enum GunHandled
{
    FireGun,
    Hook
}

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    [Header("Player General Stats")]

    public PlayerStates playerState;
    public GunHandled actualGun;
    public float maxHealth;
    public float actualHealth;
    public float maxBattery;
    public float battery;
    [Space(5)]
    [Range(0.1f, 0.5f)]
    public float rotationSpeed = 0.2f;
    public float movementSpeed = 0;
    public float accelerationPerSecond = 5;
    public int maxSpeed = 10;
    [Space(5)]
    public bool grounded;
    public float groundCheckRay = 1;
    [Space(5)]
    public GameObject fireMuzzle;
    [HideInInspector] public Rigidbody rb;

    //Red Gun

    [Header("Red Gun Stats")]
    public GameObject gunFire;
    public float gunFireHealthDamage = 20;
    public float gunFireStaminaDamage = 5;
    public float gunFireSpeed = 150;
    public float gunFireLifeTime = 3;
    public float gunFireRate = 0.1f;
    private float gunFireCDTimer = 0;
    private bool charge = true;

    //Hook Variables

    [Header("Hook Variables")]
    public float hookStaminaLoss = 10;
    [HideInInspector] public int hookCharges = 2;
    [HideInInspector] public Hook hookInGame;
    public Hook firstHook;
    public Hook secondHook;
    public float hookMaxDistance = 10;
    [HideInInspector] public bool inHook = false;
    [HideInInspector] public bool canInteract = false;
    public float hookCastTime = 0.5f;
    private float hookTimer = 0;
    public InstantObject instantObject;
    [HideInInspector] public bool canHookShot = true;
    [HideInInspector] public bool canInstantShot = false;
    public float instantObjectForce = 10;

    #region Monobehaviour

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        grounded = GroundCheck();
    }

    void Update()
    {
        switch (playerState)
        {
            case PlayerStates.Idle:

                Rotate();
                Move();

                SwapGun();

                if (Input.GetButtonDown("Fire1"))
                {
                    if (actualGun == GunHandled.FireGun)
                    {
                        GunFire();
                    }
                    else if (actualGun == GunHandled.Hook)
                    {
                        if (canHookShot)
                        {
                            HookShot();
                        }
                        else if (canInstantShot)
                        {
                            InstantShot();
                        }
                    }
                }

                if (Input.GetButtonDown("Interact") && hookInGame != null && hookInGame.collisionType != CollisionType.None)
                {
                    HookInteract();
                }

                break;
            case PlayerStates.Stunned:



                break;
            case PlayerStates.Dying:


                break;
            case PlayerStates.Died:


                break;
            case PlayerStates.HookInteracting:

                SlideToHook();

                break;
            default:
                break;
        }

        Timers();
    }

    #endregion

    public void ChangeState(PlayerStates newState)
    {
        playerState = newState;
    }

    #region Movement Methods

    public void Move()
    {
        if (grounded)
        {
            float xMove = Input.GetAxis("Horizontal");
            float zMove = Input.GetAxis("Vertical");


            Vector3 movement;

            if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
            {
                movementSpeed += Time.deltaTime * accelerationPerSecond;
                if (movementSpeed > maxSpeed)
                    movementSpeed = maxSpeed;
            }
            else
            {
                movementSpeed -= Time.deltaTime * maxSpeed / 2;
                if (movementSpeed < 0)
                    movementSpeed = 0;
            }

            movement.x = xMove * movementSpeed;
            movement.y = rb.velocity.y;
            movement.z = zMove * movementSpeed;

            rb.velocity = movement;
        }
    }

    public void Rotate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); ;
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 lookPos = new Vector3(hit.point.x, 0, hit.point.z);

            Vector3 dir = lookPos - new Vector3(transform.position.x, 0, transform.position.z);

            transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, dir, rotationSpeed, 0.0f));
        }

    }

    #endregion

    #region Fire Methods

    public void GunFire()
    {
        if (gunFireCDTimer <= 0)
        {
            GameObject g = Instantiate(gunFire, fireMuzzle.transform.position, Quaternion.Euler(0, transform.eulerAngles.y, 0));
            gunFireCDTimer = gunFireRate;
            g.GetComponent<GunProjectile>().lifeTime = gunFireLifeTime;
        }
    }

    #endregion

    #region Hook Methods

    public IEnumerator CastFirstHook()
    {
        ChangeState(PlayerStates.Stunned);
        canHookShot = false;
        rb.velocity = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(hookCastTime);
        GameObject g = Instantiate(firstHook.gameObject, fireMuzzle.transform.position, Quaternion.Euler(0, transform.eulerAngles.y, 0));
        hookInGame = g.GetComponent<Hook>();
        ChangeState(PlayerStates.Idle);
    }

    public void HookShot()
    {
        if (canHookShot)
        {
            if (hookCharges == 2)
            {
                StartCoroutine("CastFirstHook");
                hookCharges = 1;
            }
            else if (hookCharges == 1)
            {
                GameObject g = Instantiate(secondHook.gameObject, fireMuzzle.transform.position, Quaternion.Euler(0, transform.eulerAngles.y, 0));
                hookInGame.otherHook = g.GetComponent<Hook>();
                hookInGame = null;
                hookCharges = 2;
            }
            else
            {
                hookCharges = 2;
            }
        }
    }

    public void HookInteract()
    {
        switch (hookInGame.collisionType)
        {
            case CollisionType.InteractableSolid:

                ChangeState(PlayerStates.HookInteracting);

                break;
            case CollisionType.InteractableDynamic:

                DynamicInteraction();

                break;
            case CollisionType.InstantDynamic:
                break;
            case CollisionType.None:
                break;
            default:
                break;
        }
    }

    public void SlideToHook()
    {
        if (hookInGame)
        {
            rb.velocity = (-transform.position + hookInGame.transform.position) * 1.5f;
            movementSpeed = maxSpeed;

            if (Vector3.Distance(transform.position, hookInGame.transform.position) < 10)
            {
                Destroy(hookInGame.gameObject);
                ResetHook();
                ChangeState(PlayerStates.Idle);
            }
        }
    }

    public void DynamicInteraction()
    {
        if (hookInGame)
        {
            hookInGame.AddForceFromTo(hookInGame.dynamicRb, gameObject, 1000);
        }
    }

    public void InstantShot()
    {
        if(instantObject && canInstantShot)
        {
            Rigidbody instantRB = instantObject.rb;
            instantRB.constraints = RigidbodyConstraints.None;
            instantObject.shoot = true;
            instantObject.transform.SetParent(null);
            instantRB.useGravity = true;
            instantRB.AddForce(transform.forward * instantObjectForce * 600);
            ResetHook();
        }
    }

    public void ResetHook()
    {
        inHook = false;
        hookTimer = 0.5f;
        hookCharges = 2;
        hookInGame = null;
        canHookShot = true;
        canInstantShot = false;
    }

    #endregion

    #region Stats Management Methods

    public void TakeDamage(float damage)
    {
        actualHealth -= damage;
        if (actualHealth <= 0)
        {
            ChangeState(PlayerStates.Died);
        }
    }

    public IEnumerator Stun(float stun)
    {
        if (playerState != PlayerStates.Stunned)
        {
            movementSpeed = 0;
            ChangeState(PlayerStates.Stunned);
            yield return new WaitForSeconds(stun);
            ChangeState(PlayerStates.Idle);
        }
    }

    public void SwapGun()
    {
        if (hookCharges == 2)
        {
            if (Input.GetButtonDown("SwapToFireGun"))
            {
                actualGun = GunHandled.FireGun;
                Debug.Log("Fire Gun");
            }
            else if (Input.GetButtonDown("SwapToHook"))
            {
                actualGun = GunHandled.Hook;
                Debug.Log("Hook");
            }
        }
    }

    private bool GroundCheck()
    {
        Ray ray = new Ray(transform.position, Vector3.down);

        if (Physics.Raycast(ray, groundCheckRay, 1 << 9))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    public void Timers()
    {
        if (gunFireCDTimer > 0)
            gunFireCDTimer -= Time.deltaTime;
        else if (gunFireCDTimer < 0)
            gunFireCDTimer = 0;

        //if (hookTimer >= 0)
        //    hookTimer -= Time.deltaTime;
        //else if (hookTimer < 0)
        //    canShot = true;
    }
}
