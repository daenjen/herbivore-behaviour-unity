﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantObject : MonoBehaviour
{
    public float dps = 5;
    public float sps = 5;
    public float explosionDamage = 5;
    public float explosionStamina = 5;
    public float gasRadius = 2;
    public float gasLifeTime = 6;
    [HideInInspector] public bool shoot = false;
    [HideInInspector] public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (shoot)
            Destroy(gameObject);
    }
}
