﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PukeShot : MonoBehaviour
{
    public float movementSpeed = 100;

    [HideInInspector]
    public float lifeTime;
    private float timer = 0;
    public float damage;
    public float movememntSpeed;

    void Start()
    {
        
    }


    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);

        timer += Time.deltaTime;
        if (timer >= lifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<Player>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
