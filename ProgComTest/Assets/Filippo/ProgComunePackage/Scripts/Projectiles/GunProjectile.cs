﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Owner
{
    Player,
    Police
}

public class GunProjectile : MonoBehaviour
{
    public Owner owner;
    [HideInInspector] public float movementSpeed;
    [HideInInspector] public float damage;
    [HideInInspector] public float lifeTime;
    private float timer = 0;

    void Start()
    {
        switch (owner)
        {
            case Owner.Player:
                movementSpeed = GameManager.instance.player.gunFireSpeed;
                break;
            case Owner.Police:
                break;
            default:
                break;
        }
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);

        timer += Time.deltaTime;
        if (timer >= lifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Monster" && owner == Owner.Player)
        {
            other.GetComponent<Monster>().TakeDamage(GameManager.instance.player.gunFireHealthDamage);
            other.GetComponent<Monster>().LoseStamina(GameManager.instance.player.gunFireStaminaDamage);
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Police" && owner == Owner.Player)
        {
            other.GetComponent<Police>().TakeDamage(GameManager.instance.player.gunFireHealthDamage);
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Player" && owner == Owner.Police)
        {
            other.GetComponent<Player>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
