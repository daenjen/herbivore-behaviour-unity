﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HookNumber
{
    First,
    Second
}

public enum CollisionType
{
    InteractableSolid,
    InteractableDynamic,
    InstantDynamic,
    None
}

public class Hook : MonoBehaviour
{
    private Player player;
    public HookNumber hookNumber;

    [Header("Hook Stats")]

    public int moveSpeed = 50;
    public int hookLifeTIme = 5;
    [HideInInspector] public bool move = true;
    [HideInInspector] public bool backToPlayer = false;
    private float lifeTimer = 0;

    // Other GameObjects and Components
    [HideInInspector] public GameObject collision;
    [HideInInspector] public CollisionType collisionType;
    [HideInInspector] public Hook otherHook;
    [HideInInspector] public Monster monsterHooked;
    [HideInInspector] public Rigidbody dynamicRb;
    private InstantObject instantObject;
    public HookTrap trap;


    [HideInInspector] public LineRenderer lr;

    private void Start()
    {
        collisionType = CollisionType.None;
        player = GameManager.instance.player;

        if (hookNumber == HookNumber.First)
        {
            lr = GetComponent<LineRenderer>();
        }
    }

    private void Update()
    {
        if (hookNumber == HookNumber.First)
        {
            LineRendereGeneration();
            CheckDistance();
        }

        #region Hook Checks
        if (move)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        }
        else if (hookNumber == HookNumber.First && otherHook == false && player.hookCharges == 2)
        {
            Debug.Log("Hook Error");
            Destroy(gameObject);
        }
        else if (backToPlayer)
        {
            transform.LookAt(player.fireMuzzle.transform.position);
            transform.Translate(Vector3.forward * Time.deltaTime * (moveSpeed / 2));
        }
        #endregion

        if (otherHook && !otherHook.move && !trap.gameObject.activeSelf)
            if (collisionType == CollisionType.InteractableSolid && otherHook.collisionType == CollisionType.InteractableSolid)
                if (monsterHooked == null && otherHook.monsterHooked == null)
                    trap.gameObject.SetActive(true);

        #region Hook Scaling
        if(transform.parent != null)
        {
            transform.localScale = new Vector3(1, 1, 1);
            transform.localScale = new Vector3(transform.localScale.x / transform.parent.localScale.x, transform.localScale.y / transform.parent.localScale.y, transform.localScale.z / transform.parent.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        #endregion
    }

    private void OnTriggerEnter(Collider other)
    {
        if (move)
        {
            if (GameManager.instance.interactableSolid.Contains(other.gameObject.tag))
            {
                move = false;
                transform.SetParent(other.gameObject.transform);
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                collision = other.gameObject;
                collisionType = CollisionType.InteractableSolid;
                player.canHookShot = true;

                if (other.gameObject.tag == "Monster")
                {
                    if (TryGetComponent(out monsterHooked))
                        monsterHooked = GetComponent<Monster>();
                    else
                        monsterHooked = GetComponentInParent<Monster>();
                }

            }
            else if (GameManager.instance.interactableDynamic.Contains(other.gameObject.tag))
            {
                move = false;
                transform.SetParent(other.gameObject.transform);
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                collision = other.gameObject;
                collisionType = CollisionType.InteractableDynamic;
                dynamicRb = collision.gameObject.GetComponent<Rigidbody>();
                player.canHookShot = true;
            }
            else if (GameManager.instance.instantDynamic.Contains(other.gameObject.tag))
            {
                if (hookNumber == HookNumber.First)
                {
                    move = false;
                    collision = other.gameObject;
                    collisionType = CollisionType.InstantDynamic;
                    other.gameObject.transform.parent = null;
                    other.gameObject.transform.SetParent(this.transform);
                    other.gameObject.transform.localPosition = new Vector3(0, 0, 0);

                    if (other.gameObject.TryGetComponent(out instantObject))
                        instantObject = other.gameObject.GetComponent<InstantObject>();
                    else
                        instantObject = other.gameObject.GetComponentInParent<InstantObject>();

                    backToPlayer = true;
                }
            }

        }
    }

    public void LineRendereGeneration()
    {
        if (otherHook == null)
        {
            lr.SetPosition(0, GameManager.instance.player.fireMuzzle.transform.position);
            lr.SetPosition(1, transform.position);
        }
        else
        {
            lr.SetPosition(0, otherHook.transform.position);
            lr.SetPosition(1, transform.position);
        }
    }

    void CheckDistance()
    {
        if (backToPlayer)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < 2)
            {
                backToPlayer = false;
                player.canInstantShot = true;
                player.instantObject = instantObject;
                instantObject.gameObject.transform.SetParent(player.transform);
                instantObject.gameObject.transform.position = player.fireMuzzle.transform.position;

                Destroy(gameObject);
            }
        }
        else if (otherHook == null)
        {
            if (Vector3.Distance(transform.position, player.transform.position) > player.hookMaxDistance)
            {
                if (collision == null)
                {
                    player.ResetHook();
                    Destroy(gameObject);
                }
                // Player with Static Objects
                else if (collisionType == CollisionType.InteractableSolid)
                {
                    player.StartCoroutine("Stun", 0.25f);
                    player.rb.AddForce((transform.position - GameManager.instance.player.transform.position) * 20);
                }
                // Player with Dynamic Objects
                else if (collisionType == CollisionType.InteractableDynamic)
                {
                    dynamicRb.AddForce((-transform.position + GameManager.instance.player.transform.position) * 20);
                }
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, otherHook.transform.position) > player.hookMaxDistance)
            {
                if (otherHook.collision == null)
                {
                    player.ResetHook();
                    Destroy(otherHook.gameObject);
                    Destroy(gameObject);
                }
                else if ((collisionType == CollisionType.InteractableSolid && otherHook.collision.tag == "Monster") || (otherHook.collisionType == CollisionType.InteractableSolid && collision.tag == "Monster"))
                {
                    // Monster with Monster
                    if (monsterHooked != null && otherHook.monsterHooked != null)
                    {
                        if (monsterHooked.movementSpeed > otherHook.monsterHooked.movementSpeed)
                        {
                            monsterHooked.StartCoroutine("Stun", 0.5f);
                            //monsterHooked.LoseStamina();
                            AddForceFromTo(monsterHooked.rb, otherHook.monsterHooked.gameObject, monsterHooked.rb.mass * 20);

                            otherHook.monsterHooked.StartCoroutine("Stun", 1);
                            AddForceFromTo(otherHook.monsterHooked.rb, monsterHooked.gameObject, otherHook.monsterHooked.rb.mass * 20);
                        }
                        else
                        {
                            otherHook.monsterHooked.StartCoroutine("Stun", 0.5f);
                            AddForceFromTo(otherHook.monsterHooked.rb, monsterHooked.gameObject, otherHook.monsterHooked.rb.mass * 20);

                            monsterHooked.StartCoroutine("Stun", 1);
                            AddForceFromTo(monsterHooked.rb, otherHook.monsterHooked.gameObject, monsterHooked.rb.mass * 20);
                        }
                    }
                    // Monster with Static Objects
                    else if (monsterHooked != null)
                    {
                        monsterHooked.StartCoroutine("Stun", 1);
                        AddForceFromTo(monsterHooked.rb, otherHook.gameObject, monsterHooked.rb.mass * 20);
                    }
                    else if (otherHook.monsterHooked != null)
                    {
                        otherHook.monsterHooked.StartCoroutine("Stun", 1);
                        AddForceFromTo(otherHook.monsterHooked.rb, gameObject, otherHook.monsterHooked.rb.mass * 20);
                    }
                }
                // Monster with Dynamic Objects
                else if (collisionType == CollisionType.InteractableDynamic && otherHook.collision.tag == "Monster")
                {
                    AddForceFromTo(dynamicRb, otherHook.monsterHooked.gameObject, otherHook.monsterHooked.rb.mass * otherHook.monsterHooked.movementSpeed / 5);
                }
                else if (otherHook.collisionType == CollisionType.InteractableDynamic && collision.tag == "Monster")
                {
                    AddForceFromTo(otherHook.dynamicRb, monsterHooked.gameObject, monsterHooked.rb.mass * monsterHooked.movementSpeed / 5);
                }
            }
        }


    }

    public void AddForceFromTo(Rigidbody objectToKnock, GameObject target, float strength)
    {
        objectToKnock.AddForce((-objectToKnock.gameObject.transform.position + target.transform.position) * strength);
    }

    private void OnDisable()
    {
        if (monsterHooked != null)
        {
            monsterHooked.hooksAttached.Remove(this);
            if (!player.canHookShot)
            {
                player.canHookShot = true;
            }
        }
    }


}

