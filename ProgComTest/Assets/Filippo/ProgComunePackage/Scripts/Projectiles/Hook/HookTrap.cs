﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookTrap : MonoBehaviour
{
    public Hook parent;

    void Update()
    {
        Adapt();
    }

    public void Adapt()
    {
        transform.position = (parent.otherHook.transform.position + parent.transform.position) / 2;
        transform.LookAt(parent.transform);
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, Vector3.Distance(parent.transform.position, parent.otherHook.transform.position));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Monster")
        {
            other.gameObject.GetComponent<Monster>().StartCoroutine("Stun", 2);
            Destroy(parent.otherHook.gameObject);
            Destroy(parent.gameObject);
        }
    }
}
