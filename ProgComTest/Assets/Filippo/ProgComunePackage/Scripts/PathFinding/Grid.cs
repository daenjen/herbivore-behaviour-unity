﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public List<Node> path { get; set; }

    public GameObject seeker;

    [SerializeField] private LayerMask wallMask;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Vector2 gridWorldSize;
    [SerializeField] private float nodeRadius;

    private Node[,] grid;

    private float nodeDiameter;
    private int gridSizeX, gridSizeY;

    private float timer = 0;

    private void Awake()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);


        CreateGrid();

    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        if (timer >= 0.2f)
        {
            CreateGrid();
            timer = 0;
        }

        //CreateGrid();
    }

    public void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];

        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);

                bool walkable = (Physics.CheckSphere(worldPoint, 0.1f, groundMask));
                List<GameObject> avaiableObjects = new List<GameObject>();

                if (walkable)
                {
                    walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, wallMask));
                    if (walkable)
                    {
                        Collider[] hitColliders = Physics.OverlapSphere(worldPoint, nodeRadius);
                        for (int i = 0; i < hitColliders.Length; i++)
                        {
                            if (hitColliders[i].gameObject.tag == "Monster")
                            {
                                avaiableObjects.Add(hitColliders[i].gameObject);
                            }
                        }
                    }
                }

                if (avaiableObjects.Count > 0)
                {
                    grid[x, y] = new Node(walkable, worldPoint, x, y, avaiableObjects);
                }
                else
                {
                    grid[x, y] = new Node(walkable, worldPoint, x, y);
                }
            }
        }
    }

    public Node NodeFromPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x - transform.position.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.z - transform.position.z + gridWorldSize.y / 2) / gridWorldSize.y;

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

        return grid[x, y];
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                if (x == 0 & y == 0)
                {
                    continue;
                }

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

    //    if (grid != null)
    //    {
    //        //Node playerNode = NodeFromPoint(player.position);

    //        foreach (Node a in grid)
    //        {
    //            Gizmos.color = (a.walkable) ? Color.white : Color.red;

    //            //if(playerNode == a)
    //            //{
    //            //    Gizmos.color = Color.cyan;
    //            //}

    //            if (path != null)
    //            {
    //                if (path.Contains(a))
    //                {
    //                    Gizmos.color = Color.black;
    //                }
    //            }

    //            if (a.avaiableFor != null /*&& a.avaiableFor.Contains(seeker)*/)
    //            {
    //                Gizmos.color = Color.blue;
    //            }

    //            Gizmos.DrawCube(a.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
    //        }
    //    }
    //}
}
