﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool walkable { get; set; }
    public List<GameObject> avaiableFor { get; set; }
    public Vector3 worldPosition { get; set; }
    public int gCost { get; set; }
    public int hCost { get; set; }
    public int fCost { get { return gCost + hCost; } }

    public int gridX { get; set; }
    public int gridY { get; set; }

    public Node parent { get; set; }

    public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY)
    {
        walkable = _walkable;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
    }

    public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY, List<GameObject> _avaiableFor)
    {
        walkable = _walkable;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
        avaiableFor = _avaiableFor;
    }
}
