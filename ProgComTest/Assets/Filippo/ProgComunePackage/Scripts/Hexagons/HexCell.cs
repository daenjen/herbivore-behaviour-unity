﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour
{
    public string n;
    public float worldX;
    public float worldY;

    public int matrixX;
    public int matrixY;

    public bool upLake = false;
    public bool downLake = false;
    public bool upRightLake = false;
    public bool upLeftLake = false;
    public bool downRightLake = false;
    public bool downLeftLake = false;

    public int pIndex = 0;
    public int rIndex = 0;

    void Start()
    {
        //GetConfiguration(out pIndex, out rIndex);
    }

    void Update()
    {

    }

    public void GetConfiguration( out int prefabIndex, out int directionIndex)
    {
        int config = 0;

        bool[] input = { upLeftLake, downLeftLake, downLake, downRightLake, upRightLake, upLake };
            
        for (int i = 0; i < 6; i++)
        {
             config += input[i] ? (int)Mathf.Pow(2,i) : 0 ;
        }
        if (upLake)
        {
            Debug.Log("");
        }

        switch (config)
        {
            #region 1 Coast
            case 32:
                //100000
                prefabIndex = 1;
                directionIndex = 0;
                break;
            case 16:
                //010000
                prefabIndex = 1;
                directionIndex = 1;
                break;
            case 8:
                //001000
                prefabIndex = 1;
                directionIndex = 2;
                break;
            case 4:
                //000100
                prefabIndex = 1;
                directionIndex = 3;
                break;
            case 2:
                //000010
                prefabIndex = 1;
                directionIndex = 4;
                break;
            case 1:
                //000001
                prefabIndex = 1;
                directionIndex = 5;
                break;
            #endregion

            #region 2 Coasts
            case 48:
                //110000
                prefabIndex = 2;
                directionIndex = 0;
                break;
            case 24:
                //011000
                prefabIndex = 2;
                directionIndex = 1;
                break;
            case 12:
                //001100
                prefabIndex = 2;
                directionIndex = 2;
                break;
            case 6:
                //000110
                prefabIndex = 2;
                directionIndex = 3;
                break;
            case 3:
                //000011
                prefabIndex = 2;
                directionIndex = 4;
                break;
            case 33:
                //100001
                prefabIndex = 2;
                directionIndex = 5;
                break;
            #endregion

            #region 3 Coasts
            case 56:
                //111000
                prefabIndex = 3;
                directionIndex = 0;
                break;
            case 28:
                //011100
                prefabIndex = 3;
                directionIndex = 1;
                break;
            case 14:
                //001110
                prefabIndex = 3;
                directionIndex = 2;
                break;
            case 7:
                //000111
                prefabIndex = 3;
                directionIndex = 3;
                break;
            case 35:
                //100011
                prefabIndex = 3;
                directionIndex = 4;
                break;
            case 49:
                //110001
                prefabIndex = 3;
                directionIndex = 5;
                break;
            #endregion

            #region 4 Coasts
            case 60:
                //111100
                prefabIndex = 4;
                directionIndex = 0;
                break;
            case 30:
                //011110
                prefabIndex = 4;
                directionIndex = 1;
                break;
            case 15:
                //001111
                prefabIndex = 4;
                directionIndex = 2;
                break;
            case 39:
                //100111
                prefabIndex = 4;
                directionIndex = 3;
                break;
            case 51:
                //110011
                prefabIndex = 4;
                directionIndex = 4;
                break;
            case 57:
                //111001
                prefabIndex = 4;
                directionIndex = 5;
                break;
            #endregion

            default:
                directionIndex = 0;
                prefabIndex = 0;
                break;
        }


        List<int> indexes = new List<int>();
        for (int i = 0; i < 6; i++)
        {
            if (input[i])
                indexes.Add(i);
        }

        int trueCount = indexes.Count;
    }
}
