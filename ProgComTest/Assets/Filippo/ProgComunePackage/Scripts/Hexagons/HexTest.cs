﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTest : MonoBehaviour
{
    public int rows;
    public int cols;
    public float levelRay;
    public float outerRadius = 10;
    private float innerRadious;
    public HexCell[,] cellMatrix;
    public GameObject[,] cellObjectsMatrix;
    private HexCell centralCell;
    public GameObject hexagon;

    void Start()
    {
        innerRadious = outerRadius * 0.866025404f;
        cellMatrix = new HexCell[rows, cols];
        cellObjectsMatrix = new GameObject[rows, cols];
        for (int j = 0; j < cols; j++)
        {
            for (int i = 0; i < rows; i++)
            {
                HexCell cell = new HexCell();
                cell.worldY = innerRadious * 2 * i + innerRadious * j;
                cell.worldX = j * (outerRadius + CatetoCalculatior(innerRadious, outerRadius));
                cellMatrix[i, j] = cell;
                GameObject g = Instantiate(hexagon, new Vector3(cell.worldX, 0, cell.worldY), Quaternion.identity);
                cellObjectsMatrix[i, j] = g;
                //g.hideFlags = HideFlags.HideInHierarchy;
            }
        }

        centralCell = cellMatrix[Mathf.FloorToInt(rows / 2), Mathf.FloorToInt(cols / 2)];

        //for (int j = 0; j < cols; j++)
        //{
        //    for (int i = 0; i < rows; i++)
        //    {
        //        if(Vector3.Distance(new Vector3(cellMatrix[i,j].x,0, cellMatrix[i, j].z), new Vector3(centralCell.x,0,centralCell.z)) > 5.25f)
        //        {
        //            Destroy(cellObjectsMatrix[i, j]);
        //        }
        //    }
        //}
    }


    void Update()
    {

    }

    public float CatetoCalculatior(float cat1, float ipo)
    {
        float cat2 = Mathf.Sqrt((ipo * ipo) - (cat1 * cat1));
        return cat2;
    }
}
