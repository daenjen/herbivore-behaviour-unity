﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexWithLakes : MonoBehaviour
{
    public int rows;
    public int cols;
    private float outerRadius;
    private float innerRadius;
    public int[,] intMatrix;
    public HexCell[,] cellMatrix;
    public GameObject[,] cellObjectsMatrix;
    private HexCell centralCell;
    public GameObject[] hexagons;


    void Start()
    {
        outerRadius = 0.5f;
        innerRadius = outerRadius * 0.866025404f;
        intMatrix = new int[rows, cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                intMatrix[i, j] = 1;
            }
        }

        int randX = Random.Range(rows - (rows - 4), rows - 4);
        int randY = Random.Range(cols - (cols - 4), cols - 4);

        intMatrix[randX, randY] = 2;

        //intMatrix[10, 10] = 2;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                if (i < rows - 4 && j < cols - 4 && i > 3 && j > 3 && intMatrix[i, j] != 2 && LakeFinding(intMatrix, i, j))
                {
                    if (Random.Range(0, 2) == 1)
                        intMatrix[i, j] = 2;
                }
            }
        }

        cellMatrix = new HexCell[rows, cols];
        cellObjectsMatrix = new GameObject[rows, cols];
        for (int j = 0; j < cols; j++)
        {
            for (int i = 0; i < rows; i++)
            {
                HexCell cell = new HexCell();
                cell.matrixX = i;
                cell.matrixY = j;
                cell.worldY = innerRadius * 2 * j + innerRadius * i;
                cell.worldX = i * (outerRadius + CatetoCalculator(innerRadius, outerRadius));
                LakeCheckList(intMatrix, cell);
                cell.GetConfiguration(out cell.pIndex, out cell.rIndex);
                cell.n = cell.matrixX + " " + cell.matrixY;
                cellMatrix[i, j] = cell;
                if (intMatrix[i, j] != 2)
                {
                    if (cell.pIndex != 0)
                    {
                        GameObject g = Instantiate(hexagons[cell.pIndex], new Vector3(cell.worldX, 0, cell.worldY), Quaternion.Euler(0, cell.rIndex * 60, 0));
                        g.name = cell.n;
                        cellObjectsMatrix[i, j] = g;
                    }
                    else
                    {
                        GameObject g = Instantiate(hexagons[0], new Vector3(cell.worldX, 0, cell.worldY), Quaternion.identity);
                        g.name = cell.n;
                        cellObjectsMatrix[i, j] = g;
                    }
                }
                //g.hideFlags = HideFlags.HideInHierarchy;
            }
        }


    }

    void Update()
    {

    }

    bool LakeFinding(int[,] matrix, int row, int col)
    {
        if (matrix[row, col + 1] == 2 || matrix[row + 1, col] == 2 || matrix[row, col - 1] == 2 || matrix[row - 1, col] == 2 || matrix[row - 1, col + 1] == 2 || matrix[row + 1, col - 1] == 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void LakeCheckList(int[,] matrix, HexCell cell)
    {
        if (cell.matrixY < cols - 1 && matrix[cell.matrixX, cell.matrixY + 1] == 2)
        {
            cell.upLake = true;
        }
        if (cell.matrixX < rows - 1 && matrix[cell.matrixX + 1, cell.matrixY] == 2)
        {
            cell.upRightLake = true;
        }
        if (cell.matrixY > 0 && matrix[cell.matrixX, cell.matrixY - 1] == 2)
        {
            cell.downLake = true;
        }
        if (cell.matrixX > 0 && matrix[cell.matrixX - 1, cell.matrixY] == 2)
        {
            cell.downLeftLake = true;
        }
        if (cell.matrixX > 0 && cell.matrixY < cols - 1 && matrix[cell.matrixX - 1, cell.matrixY + 1] == 2)
        {
            cell.upLeftLake = true;
        }
        if (cell.matrixX < rows - 1 && cell.matrixY > 0 && matrix[cell.matrixX + 1, cell.matrixY - 1] == 2)
        {
            cell.downRightLake = true;
        }
    }

    public float CatetoCalculator(float cat1, float ipo)
    {
        float cat2 = Mathf.Sqrt((ipo * ipo) - (cat1 * cat1));
        return cat2;
    }

    //public float GetRotationLake()
    //{

    //} 
}
